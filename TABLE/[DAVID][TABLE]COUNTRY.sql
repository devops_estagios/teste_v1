
DECLARE
    w_colunas ga_install_objects.my_columns;
BEGIN   
    w_Colunas(w_colunas.count).column_name := 'ID';
    w_Colunas(w_colunas.count-1).column_type := 'NUMBER';
    w_Colunas(w_colunas.count-1).column_length := null;
    w_Colunas(w_colunas.count-1).column_precision := 10;
    w_Colunas(w_colunas.count-1).column_scale := 0;
    w_Colunas(w_colunas.count-1).nullable := false;
    w_Colunas(w_colunas.count-1).column_comment := q'{}';
    w_Colunas(w_colunas.count).column_name := 'COUNTRY';
    w_Colunas(w_colunas.count-1).column_type := 'VARCHAR2';
    w_Colunas(w_colunas.count-1).column_length := 50;
    w_Colunas(w_colunas.count-1).column_precision := null;
    w_Colunas(w_colunas.count-1).column_scale := null;
    w_Colunas(w_colunas.count-1).nullable := true;
    w_Colunas(w_colunas.count-1).column_comment := q'{}';
    w_Colunas(w_colunas.count).column_name := 'LAST_UPDATE';
    w_Colunas(w_colunas.count-1).column_type := 'TIMESTAMP(0)';
    w_Colunas(w_colunas.count-1).column_length := null;
    w_Colunas(w_colunas.count-1).column_precision := null;
    w_Colunas(w_colunas.count-1).column_scale := null;
    w_Colunas(w_colunas.count-1).nullable := true;
    w_Colunas(w_colunas.count-1).column_comment := q'{}';
    ga_install_objects.create_alter_table('COUNTRY', w_colunas);
END;
/
DECLARE
  w_columns   DBMS_SQL.varchar2a;
BEGIN
  w_columns (1) := 'ID'; 
  ga_install_objects.create_primary_key (i_table_name => 'COUNTRY', i_pk_name => 'SYS_C003805924', i_pk_columns => w_columns, i_deferrable => FALSE, i_deferred => FALSE);
END;
/
DECLARE
  l_index_columns   DBMS_SQL.varchar2a;
BEGIN 
   l_index_columns (1) := 'ID'; 
  ga_install_objects.create_unique_index (i_table_name => 'COUNTRY', i_index_name => 'SYS_C003805924', i_index_columns => l_index_columns);
END;
/
