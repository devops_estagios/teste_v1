
  CREATE OR REPLACE PACKAGE "PCK_GA_EXTENSION" IS

    PROCEDURE create_SYNONYM(
        i_synonym_name    IN VARCHAR2,
        i_object_owner    IN VARCHAR2,
        i_object_name     IN VARCHAR2
    );

    PROCEDURE give_GRANT(
        i_object_name     IN VARCHAR2,
        i_privileges      IN VARCHAR2,
        i_grantee         IN VARCHAR2,
        i_grant_option    IN VARCHAR2,
        o_error           OUT VARCHAR2
    );

    FUNCTION get_USER(
        i_user            IN VARCHAR2
    ) RETURN VARCHAR2;

END PCK_GA_EXTENSION;

/
CREATE OR REPLACE PACKAGE BODY "PCK_GA_EXTENSION" IS

-- do not change this package
-- with this extension, you can ask for grants from other schemas

--**************************************************************************************************
    PROCEDURE create_SYNONYM(
        i_synonym_name    IN VARCHAR2,
        i_object_owner    IN VARCHAR2,
        i_object_name     IN VARCHAR2
    ) IS
    BEGIN
        ga.GA_INSTALL_OBJECTS.create_synonym(
            i_synonym_owner   => sys_context('USERENV','CURRENT_SCHEMA'),
            i_synonym_name    => nvl(i_synonym_name,i_object_name),
            i_object_owner    => i_object_owner,
            i_object_name     => i_object_name
        );
    END create_SYNONYM;

--**************************************************************************************************
    PROCEDURE give_GRANT(
        i_object_name     IN VARCHAR2,
        i_privileges      IN VARCHAR2,
        i_grantee         IN VARCHAR2,
        i_grant_option    IN VARCHAR2,
        o_error           OUT VARCHAR2
    ) IS
        l_sql VARCHAR2(4000);
    BEGIN

        <<VALIDATIONS>>
        DECLARE
            l_count NUMBER;
        BEGIN

            IF nvl(length(i_privileges),0) <=1
            THEN
                o_error := 'No privileges to grant';
                RETURN;
            END IF;

            SELECT COUNT(1)
              INTO l_count
              FROM USER_OBJECTS
             WHERE OBJECT_NAME = i_object_name;

            IF l_count = 0
            THEN
                o_error := 'Object ['||i_object_name||'] does not exist';
                RETURN;
            END IF;

            SELECT COUNT(1)
              INTO l_count
              FROM ALL_USERS
             WHERE USERNAME = i_grantee;

            IF l_count = 0
            THEN
                o_error := 'Grantee user ['||i_grantee||'] does not exist';
                RETURN;
            END IF;

        END VALIDATIONS;

        <<SQL_CONSTRUCTION>>
        BEGIN
            l_sql := 'grant '||i_privileges||' on '||sys_context('USERENV', 'CURRENT_SCHEMA')||'.'||i_object_name||' to '||i_grantee||CASE WHEN i_grant_option = 'S' THEN ' WITH GRANT OPTION' ELSE '' END;
        END SQL_CONSTRUCTION;

        GA.GA_INSTALL_OBJECTS.EXECUTE_IMMEDIATE(I_SQL => l_sql);

    EXCEPTION WHEN OTHERS THEN
        o_error := SQLERRM;
        RAISE;
    END give_GRANT;

--**************************************************************************************************
    FUNCTION get_USER(
        i_user      IN VARCHAR2
    ) RETURN VARCHAR2 IS
        o_user      VARCHAR2(30);
    BEGIN

        IF sys_context('USERENV','CURRENT_SCHEMA') = i_user
        THEN
            o_user := i_user;
        ELSIF sys_context('USERENV','CURRENT_SCHEMA') = 'PH_LG' AND i_user IN ('PH','LG')
        THEN
            EXECUTE IMMEDIATE
            'SELECT USER_DE_LIGACAO
               FROM PH_LG_P_INSTALACAO_USER
              WHERE USER_PH = :1'
            INTO o_user
            USING IN CASE WHEN i_user = 'PH' THEN 'S' ELSE 'N' END;

        END IF;

        RETURN o_user;

    EXCEPTION WHEN OTHERS THEN RAISE NO_DATA_FOUND;
    END get_USER;

END PCK_GA_EXTENSION;

/